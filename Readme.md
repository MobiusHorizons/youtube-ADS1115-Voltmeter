# ADS1115 Voltmeter

This is a simple voltmeter project to illustrate using **I2c** on an **ESP8266** in the Arduino environment.

# Dependencies
It depends on the **ADS1X15** library from Adafruit, and the **U8g2lib** library from oliver.

# Setup
For this project, we are conneting the **ADS1115** and the OLED display over **I2C**. Please note, the **ADS1115**
requires 5v in order to work correctly in this configuration, so make sure you plug the `VCC` pin of the **ADS1115** to
`5v`

The general overview of the wiring for this project can be seen here

![Overview](overview.jpg)

## Wemos D1 Mini
The Wemos D1 Mini is connected to power over micro usb. The wiring is as folows:

The **Red** wire is connected to `5v`.
The **Black** wire is connect to `GND`.
The **Blue** wire is connected to `D1` which acts as the `SCL` pin for the **I2C** Bus.
The **Yellow** wire is conneected to `D2` which acts as the `SDA` pin for the **I2C** bus.

![Wemos](Wemos D1 Mini.jpg)

## ADS1115

The ADS1115 is connected to the **I2C** bus, and is measuring voltage between `A0` and `A1`. 

The **Red** wire is connected to the `V` or `VCC` pin.  
The **Black** wire is connected to the `G` or `GND` pin.  
The **Blue** wire is connected to the `SCL` pin.
The **Yellow** wire is connected to the `SDA` pin.

There are also an extra pair of **Red** and **Black** wires connected to the `A0` and `A1` pins. These two wires are
used as probes to measure a voltage.

![ADS1115](ADS1115.jpg)

## OLED display
The 32x128 OLED display is connected to the **I2C** Bus. You will notice that the order of `VCC` and `GND` is the
opposite of the **ADS1115** sensor, so make sure you look at the letters on the board before you plug the power in.

The **Black** wire is connected to the `GND` pin.  
The **Red** wire is connected to the `VCC` pin.  
The **Blue** wire is connected to the `SCL` pin.
The **Yellow** wire is connected to the `SDA` pin.

![OLED](OLED.jpg)


## Serial
Serial is set to 9600 Baud on this project, and you can view the output in `mV` using the **Serial Plotter** option.
